package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_overview.*
import webdealer.nouwe.R

class Overview : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_overview)
        super.onCreate(savedInstanceState)

    }

    override fun init() {
        setToolbarTitle("OVERVIEW")
        addPMethod.setOnClickListener {
            addPaymentUI.visibility = View.GONE
            confirmBookingUI.visibility = View.VISIBLE
            startActivity(Intent(it.context, PaymentMethods::class.java))
        }
        confirmBooking.setOnClickListener {
            startActivity(Intent(it.context, Booking::class.java))
        }

    }
}
