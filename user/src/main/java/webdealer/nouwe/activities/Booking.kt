package webdealer.nouwe.activities

import android.app.Dialog
import android.os.Bundle
import android.view.Window
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_booking.*
import kotlinx.android.synthetic.main.cancel_search_dialog.*
import kotlinx.android.synthetic.main.congrats_dialog.*
import webdealer.nouwe.R

class Booking : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_booking)
        super.onCreate(savedInstanceState)

    }

    override fun init() {
        setToolbarTitle("PAIRING")
        cancelReq.setOnClickListener {
            cancelRequestDialog()
        }

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressTV.text = progress.toString() + "%"
                if (progress == 0)
                    bookingFailedDialog()
                else if (progress >= 50)
                    bookingSuccessDialog()
                else if (progress < 50)
                    bookingPendingDialog()


            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })
    }

    private fun bookingSuccessDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.congrats_dialog)
        dialog.imageView.setImageResource(R.drawable.greentick)
        dialog.content.text = "Your book has been confirmed.You will be able to contact the Visual Agent to prep for your tour."
        dialog.header.text = "BOOKING SUCCESSFUL"
        dialog.done.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun bookingFailedDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.congrats_dialog)
        dialog.imageView.setImageResource(R.drawable.error)
        dialog.content.text = "Sorry there are know Visual Agents available for the time frame or location you provided.Please try again with a different search.Thank you."
        dialog.header.text = "BOOKING UNSUCCESSFUL"
        dialog.done.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun bookingPendingDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.congrats_dialog)
        dialog.imageView.setImageResource(R.drawable.pending)
        dialog.content.text = "Your booking is pending.It will be automatically cancelled if a Visual Agent does not accept within a certain timeframe.You will be notified."
        dialog.header.text = "BOOKING PENDING"
        dialog.done.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    private fun cancelRequestDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.cancel_search_dialog)
        dialog.show()
        dialog.yesC.setOnClickListener {
            dialog.dismiss()
            onBackPressed()
        }
        dialog.noC.setOnClickListener {
            dialog.dismiss()
        }
    }
}
