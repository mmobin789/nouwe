package webdealer.nouwe.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_verification.*
import kotlinx.android.synthetic.main.congrats_dialog.*
import kotlinx.android.synthetic.main.terms_conditions_dialog.*
import webdealer.nouwe.R
import webdealer.nouwe.commonstuff.utils.AndroidUtils

class Verification : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_verification)
        super.onCreate(savedInstanceState)
    }


    override fun init() {
        val updatePhone = intent.getBooleanExtra("update", false)
        setToolbarTitle("Verification")
        submit.setOnClickListener {
            if (updatePhone)
                updatePhone()
            else
                showTermsDialog(it)
        }
        changeNumber.setOnClickListener {
            onBackPressed()
        }
    }

    private fun updatePhone() {

    }

    private fun showTermsDialog(v: View) {
        setToolbarTitle("Agreement")
        verificationUI.visibility = View.GONE
        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.terms_conditions_dialog)
        dialog.termsTV.setText(AndroidUtils.getSpannableText(getString(R.string.terms)), TextView.BufferType.SPANNABLE)
        dialog.show()
        dialog.ok.setOnClickListener {
            dialog.dismiss()
            successDialog(it)
        }
        dialog.setOnCancelListener {
            setToolbarTitle("Verification")
            verificationUI.visibility = View.VISIBLE
        }

    }

    private fun successDialog(v: View) {
        setToolbarTitle("Success")
        verificationUI.visibility = View.GONE
        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.congrats_dialog)
        dialog.show()
        dialog.done.setOnClickListener {
            startActivity(Intent(it.context, MainActivity::class.java))
            dialog.dismiss()
        }
        dialog.setOnCancelListener {
            setToolbarTitle("Verification")
            verificationUI.visibility = View.VISIBLE
        }
    }
}
