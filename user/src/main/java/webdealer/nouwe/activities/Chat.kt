package webdealer.nouwe.activities

import android.os.Bundle
import webdealer.nouwe.R

class Chat : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_chat)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        setToolbarTitle("MESSAGING")
    }
}
