package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.*
import webdealer.nouwe.R

class Login : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_login)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        setToolbarTitle("Login")
        forgotPassword.setOnClickListener {
            startActivity(Intent(it.context, ForgotPassword::class.java))
        }
        login.setOnClickListener {
            startActivity(Intent(it.context, MainActivity::class.java))
        }
    }
}
