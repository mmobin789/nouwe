package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import kotlinx.android.synthetic.main.activity_billing_address.*
import webdealer.nouwe.R

class BillingAddress : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_billing_address)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        setToolbarTitle("ADD PAYMENT METHOD")
        done.setOnClickListener {
            startActivity(Intent(it.context, ChoosePaymentMethod::class.java))
            onBackPressed()
        }
        etState.setOnClickListener {
            showStates(it)
        }
    }

    private fun showStates(v: View) {
        val popupMenu = PopupMenu(v.context, v)
        val menu = popupMenu.menu
        menu.add("State 1")
        menu.add("State 2")
        menu.add("State 3")
        popupMenu.show()
        popupMenu.setOnMenuItemClickListener {
            etState.setText(it.title)
            true
        }
    }
}
