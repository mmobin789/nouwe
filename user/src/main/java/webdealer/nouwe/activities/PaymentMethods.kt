package webdealer.nouwe.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Window
import kotlinx.android.synthetic.main.activity_payment_methods.*
import kotlinx.android.synthetic.main.paypal_dialog.*
import webdealer.nouwe.R

class PaymentMethods : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_payment_methods)
        super.onCreate(savedInstanceState)

    }

    override fun init() {
        setToolbarTitle("PAYMENT METHOD")
        cc.setOnClickListener {
            startActivity(Intent(it.context, AddCreditCard::class.java))
            onBackPressed()
        }
        pp.setOnClickListener {
            paypalDialog()
        }
    }

    private fun paypalDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.paypal_dialog)
        dialog.done.setOnClickListener {
            startActivity(Intent(it.context, ChoosePaymentMethod::class.java))
        }
        dialog.cancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
}
