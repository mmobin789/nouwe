package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_welcome.*
import webdealer.nouwe.R

class Welcome : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_welcome)
        super.onCreate(savedInstanceState)

    }

    override fun init() {
        val legal = intent.getBooleanExtra("legal", false)

        ok.setOnClickListener {
            startActivity(Intent(it.context, Auth::class.java))
        }
        if (legal) {
            val legalDetail = intent.getStringExtra("detail")
            ok.visibility = View.GONE
            tb.visibility = View.VISIBLE
            setToolbarTitle(legalDetail)
        }

    }
}
