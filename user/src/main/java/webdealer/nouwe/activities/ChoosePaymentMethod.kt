package webdealer.nouwe.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_choose_payment.*
import kotlinx.android.synthetic.main.tip_menu.*
import webdealer.nouwe.R
import webdealer.nouwe.adapter.PaymentMethodAdapter
import webdealer.nouwe.models.PaymentMethod

class ChoosePaymentMethod : Base() {


    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_choose_payment)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        setToolbarTitle("PAYMENT METHOD")
        tipMessage.text = "Press and hold a payment method to delete it"
        tipMessage.typeface = Typeface.DEFAULT
        close.setOnClickListener {
            label.visibility = View.GONE
        }
        rv.layoutManager = LinearLayoutManager(this)

        rv.adapter = PaymentMethodAdapter(addPaymentOptions())

        addPMethod.setOnClickListener {
            startActivity(Intent(it.context, PaymentMethods::class.java))
            onBackPressed()
        }
    }

    private fun addPaymentOptions(): List<PaymentMethod> {
        val list = mutableListOf<PaymentMethod>()
        val paypal = PaymentMethod("Paypal", R.drawable.paypalicon)
        val creditCard = PaymentMethod("Credit/Debit", R.drawable.creditcard)
        creditCard.isSelected = true
        list.add(paypal)
        list.add(creditCard)
        return list
    }


}
