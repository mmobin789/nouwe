package webdealer.nouwe.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Window
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.congrats_dialog.*
import webdealer.nouwe.R

class ForgotPassword : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_forgot_password)
        super.onCreate(savedInstanceState)


    }

    override fun init() {
        setToolbarTitle("Forgot Password")
        reset.setOnClickListener {
            sentDialog()
        }
    }

    private fun sentDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.congrats_dialog)
        dialog.imageView.setImageResource(R.drawable.greentick)
        dialog.header.text = "SENT"
        dialog.content.text = "An email has been sent with instructions,Please check your email or phone."
        dialog.show()
        dialog.done.setOnClickListener {
            dialog.dismiss()
            startActivity(Intent(it.context, ResetPassword::class.java))


        }
    }
}
