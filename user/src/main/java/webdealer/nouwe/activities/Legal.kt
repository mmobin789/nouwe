package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_legal.*
import webdealer.nouwe.R

class Legal : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_legal)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        setToolbarTitle("LEGAL")
        terms.setOnClickListener {
            legalDetail("TERMS & CONDITIONS")
        }
        policy.setOnClickListener {
            legalDetail("POLICY")
        }
        code.setOnClickListener {
            legalDetail("CODE OF CONDUCT")
        }
    }

    private fun legalDetail(header: String) {
        val legalDetail = Intent(this, Welcome::class.java)
        legalDetail.putExtra("legal", true)
        legalDetail.putExtra("detail", header)
        startActivity(legalDetail)
    }
}
