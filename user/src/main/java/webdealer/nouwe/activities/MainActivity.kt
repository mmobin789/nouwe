package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.view.Gravity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.drawer.*
import kotlinx.android.synthetic.main.toolbar.*
import webdealer.nouwe.R
import webdealer.nouwe.commonstuff.utils.AndroidUtils
import webdealer.nouwe.fragments.*

class MainActivity : Base() {
    private lateinit var baseUI: BaseUI
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_main)
        super.onCreate(savedInstanceState)

    }

    override fun init() {


        //    val user = AppStorage.getUser()

        val navClickEvent = View.OnClickListener {
            baseUI = when (it.id) {

                help.id -> {
                    toolbarDesign("HELP", false)
                    Help.newInstance()
                }
                settings.id -> {
                    toolbarDesign("SETTINGS", false)
                    Settings.newInstance()
                }
                history.id -> {
                    toolbarDesign("HISTORY", false)
                    History.newInstance()
                }


                messages.id -> {
                    toolbarDesign("MESSAGING", false)
                    Messages.newInstance()
                }
                else -> {
                    toolbarDesign("", true)
                    Home.newInstance()
                }
            }
            drawerL.closeDrawer(Gravity.START, true)
            setFragment(baseUI)
        }
        toolbarDesign("", true)
        setFragment(Home.newInstance())
        messages.setOnClickListener(navClickEvent)
        settings.setOnClickListener(navClickEvent)
        help.setOnClickListener(navClickEvent)
        history.setOnClickListener(navClickEvent)
        editProfileTV.setOnClickListener { startActivity(Intent(it.context, EditProfile::class.java)) }
        logout.setOnClickListener { onBackPressed() }
        banking.setOnClickListener { startActivity(Intent(it.context, ChoosePaymentMethod::class.java)) }

        drawerIV.setOnClickListener {

            drawerL.openDrawer(Gravity.START)
        }

        AndroidUtils.loadWithGlide(R.drawable.white_bg, drawer_bg, false)
    }

    private fun closeDrawer() {
        if (drawerL.isDrawerOpen(Gravity.START))
            drawerL.closeDrawer(Gravity.START)
    }

    fun setFragment(baseUI: BaseUI) {
        closeDrawer()
        this.baseUI = baseUI
        supportFragmentManager.beginTransaction().replace(R.id.container, this.baseUI).commit()
    }

    fun toolbarDesign(header: String, isHome: Boolean) {
        setToolbarTitle(header)
        drawerL.setDrawerLockMode(if (isHome) {
            drawerIV.visibility = View.VISIBLE
            logo.visibility = View.VISIBLE
            titleTB.visibility = View.GONE
            back.visibility = View.GONE
            DrawerLayout.LOCK_MODE_UNLOCKED
        } else {
            back.visibility = View.VISIBLE
            titleTB.visibility = View.VISIBLE
            logo.visibility = View.GONE
            drawerIV.visibility = View.GONE
            DrawerLayout.LOCK_MODE_LOCKED_CLOSED
        })
    }

    override fun onBackPressed() {
        if (baseUI is Home)
            super.onBackPressed()
        else
            baseUI.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        baseUI.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        baseUI.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


}
