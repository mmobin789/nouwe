package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_sign_up.*
import webdealer.nouwe.R
import webdealer.nouwe.commonstuff.utils.AndroidUtils

class SignUp : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_sign_up)
        super.onCreate(savedInstanceState)
        init()
    }

    override fun init() {
        setToolbarTitle("Register")
        signup.setOnClickListener {
            startActivity(Intent(it.context, Verification::class.java))
        }
        AndroidUtils.loadWithGlide(R.drawable.white_bg, bgIV, false)
    }
}
