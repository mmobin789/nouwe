package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_tour_completed.*
import kotlinx.android.synthetic.main.toolbar.*
import webdealer.nouwe.R

class TourCompleted : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_tour_completed)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        setToolbarTitle("TOUR SUMMARY")
        back.visibility = View.INVISIBLE
        rate.setOnClickListener {
            startActivity(Intent(it.context, Rating::class.java))
            onBackPressed()
        }
    }
}
