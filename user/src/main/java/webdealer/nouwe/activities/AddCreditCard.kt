package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import kotlinx.android.synthetic.main.activity_add_credit_card.*
import webdealer.nouwe.R
import webdealer.nouwe.adapter.CreditCardAdapter
import webdealer.nouwe.commonstuff.utils.AndroidUtils

class AddCreditCard : Base(), ViewPager.OnPageChangeListener {
    private lateinit var creditCardAdapter: CreditCardAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_add_credit_card)
        super.onCreate(savedInstanceState)

    }

    override fun init() {
        pager.offscreenPageLimit = 3
        creditCardAdapter = CreditCardAdapter(supportFragmentManager)
        pager.adapter = creditCardAdapter
        pager.addOnPageChangeListener(this)
        AndroidUtils.loadWithGlide(R.drawable.card1, cardIV, false)
        setToolbarTitle("ADD PAYMENT")
        next.setOnClickListener {
            if (next.text == "DONE") {
                startActivity(Intent(it.context, BillingAddress::class.java))
                onBackPressed()
            } else if (pager.currentItem < creditCardAdapter.count) pager.currentItem = pager.currentItem + 1
        }


    }


    override fun onPageSelected(position: Int) {
        next.text = "Next"
        val res = when (position) {
            0 -> {

                R.drawable.card1
            }
            1 -> {

                R.drawable.card2
            }
            2 -> {

                R.drawable.card3
            }
            else -> {
                next.text = "DONE"
                R.drawable.card4
            }
        }
        AndroidUtils.loadWithGlide(res, cardIV, false)
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageScrollStateChanged(state: Int) {

    }
}
