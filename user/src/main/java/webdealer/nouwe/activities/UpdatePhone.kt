package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_update_phone.*
import webdealer.nouwe.R
import webdealer.nouwe.commonstuff.utils.AndroidUtils

class UpdatePhone : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_update_phone)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        setToolbarTitle("UPDATE PHONE NUMBER")
        submit.setOnClickListener {
            val updatePhone = Intent(it.context, Verification::class.java)
            updatePhone.putExtra("update", true)
            startActivity(updatePhone)
        }
        AndroidUtils.loadWithGlide(R.drawable.white_bg, bgIV, false)
    }
}
