package webdealer.nouwe.activities

import android.content.Intent
import android.os.Bundle
import webdealer.nouwe.R
import webdealer.nouwe.commonstuff.utils.AndroidUtils.delay
import webdealer.nouwe.persistence.AppStorage

class Splash : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_splash)
        super.onCreate(savedInstanceState)

    }

    override fun init() {
        delay(3, Runnable {
            val ui = if (AppStorage.isAppLaunched1stTime()) {
                AppStorage.set1stTimeAppLaunched(false)
                Welcome::class.java
            } else Auth::class.java

            startActivity(Intent(this, ui))
            onBackPressed()
        })
    }
}
