package webdealer.nouwe.models

data class PaymentMethod(val name: String, val image: Int) {
    var isSelected = false
}