package webdealer.nouwe.models

class Reservation {
    private var isSwipedUP = false
    fun getActive() = isSwipedUP
    fun setActive(active: Boolean): Reservation {
        isSwipedUP = active
        return this
    }
}