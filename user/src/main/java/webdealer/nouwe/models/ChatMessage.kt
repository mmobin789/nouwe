package webdealer.nouwe.models

import webdealer.nouwe.commonstuff.utils.Utils

class ChatMessage(val isSelf: Boolean, val message: String, val avatar: String) {
    val timestamp = Utils.getCurrentTime()
}