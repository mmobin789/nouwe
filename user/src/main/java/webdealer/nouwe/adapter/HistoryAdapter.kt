package webdealer.nouwe.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import webdealer.nouwe.R

class HistoryAdapter : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.history_row, p0, false))
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

    }

}