package webdealer.nouwe.adapter

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import webdealer.nouwe.fragments.*

class CreditCardAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): BaseUI {
        return when (position) {
            0 -> AddCC1.newInstance()
            1 -> AddCC2.newInstance()
            2 -> AddCC3.newInstance()
            else -> AddCC4.newInstance()
        }
    }

    override fun getCount(): Int {
        return 4
    }
}