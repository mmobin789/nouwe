package webdealer.nouwe.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import webdealer.nouwe.R
import webdealer.nouwe.activities.Chat

class UpcomingAdapter : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.upcoming_row, p0, false))
        vh.containerView.setOnClickListener {
            it.context.startActivity(Intent(it.context, Chat::class.java))
        }
        return vh
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

    }

}