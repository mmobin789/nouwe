package webdealer.nouwe.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.va_row.*
import webdealer.nouwe.R
import webdealer.nouwe.models.Reservation

class ReservationsAdapter(private val list: List<Reservation>) : RecyclerView.Adapter<ViewHolder>() {


    private var lastCheckedPosition = 0

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.va_row, p0, false))


        vh.containerView.setOnLongClickListener {
            list[lastCheckedPosition].setActive(false)
            notifyItemChanged(lastCheckedPosition)
            list[vh.adapterPosition].setActive(true)
            notifyItemChanged(vh.adapterPosition)
            lastCheckedPosition = vh.adapterPosition
            true
        }
        vh.cancelBooking.setOnClickListener {
            Toast.makeText(it.context, vh.adapterPosition.toString(), Toast.LENGTH_SHORT).show()
            //cancelBookingDialog(it.context)
        }
        return vh
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val reservation = list[p1]

        p0.detailsUI.visibility = if (reservation.getActive())
            View.VISIBLE
        else View.GONE


    }


}