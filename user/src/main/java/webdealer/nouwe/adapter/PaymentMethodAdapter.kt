package webdealer.nouwe.adapter

import android.app.Dialog
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import kotlinx.android.synthetic.main.congrats_dialog.*
import kotlinx.android.synthetic.main.payment_method_row.*
import webdealer.nouwe.R
import webdealer.nouwe.models.PaymentMethod

class PaymentMethodAdapter(private val list: List<PaymentMethod>) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.payment_method_row, p0, false))
        vh.containerView.setOnLongClickListener {
            deletePaymentMethodDialog(it.context)
            true
        }
        return vh
    }

    private fun deletePaymentMethodDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.congrats_dialog)
        dialog.imageView.setImageResource(R.drawable.error)
        dialog.header.text = "DELETE METHOD?"
        dialog.content.text = "Are you sure you want to delete this payment method ?"
        dialog.done.visibility = View.GONE
        dialog.yesNoUI.visibility = View.VISIBLE
        dialog.yes.setOnClickListener {

        }
        dialog.no.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val paymentMethod = list[p1]
        p0.name.text = paymentMethod.name
        p0.pimg.setImageResource(paymentMethod.image)
        if (paymentMethod.isSelected)
            p0.selectIV.setImageResource(R.drawable.checked)
        else p0.selectIV.visibility = View.INVISIBLE
    }

}