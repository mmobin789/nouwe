package webdealer.nouwe.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import webdealer.nouwe.R
import webdealer.nouwe.models.ChatMessage

class ChatAdapter : RecyclerView.Adapter<ViewHolder>() {
    private val uid: Int = 0//chatActivity.userID
    private val user = 0
    private val va = 1
    private val list = mutableListOf<ChatMessage>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val layout = if (p1 == user) {
            R.layout.sender
        } else {
            R.layout.receiver
        }
        return ViewHolder(LayoutInflater.from(p0.context).inflate(layout, p0, false))
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

    }

    override fun getItemViewType(position: Int): Int {
        val chatMessage = list[position]
        return if (chatMessage.isSelf)
            user
        else va
    }

}