package webdealer.nouwe.persistence

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson

object AppStorage {
    private lateinit var prefs: SharedPreferences
    private val gson = Gson()
    private const val key1st = "1st"
    fun init(context: Context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun set1stTimeAppLaunched(firstTime: Boolean) {

        prefs.edit().putBoolean(key1st, firstTime).apply()
    }

    fun isAppLaunched1stTime() = prefs.getBoolean(key1st, true)

    fun getFCM() = prefs.getString("fcm", "")

    fun setFCM(token: String) = prefs.edit().putString("fcm", token).apply()

    //  fun getUser() = gson.fromJson(prefs.getString(keyUser, ""), User::class.java)

    fun clearSession() = prefs.edit().clear().apply()

    //  fun isLoggedIn() = prefs.getString(keyUser, "").isNotBlank()


}