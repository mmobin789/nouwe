package webdealer.nouwe.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_cc.*
import webdealer.nouwe.R

class AddCC1 : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_cc, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        nameCC()
    }

    private fun nameCC() {
        label.text = " NAME ON CREDIT CARD"
    }


    companion object {
        fun newInstance() = AddCC1()
    }
}