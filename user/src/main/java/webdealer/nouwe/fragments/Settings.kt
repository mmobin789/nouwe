package webdealer.nouwe.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_settings.*
import webdealer.nouwe.R
import webdealer.nouwe.activities.SetPinCode
import webdealer.nouwe.commonstuff.utils.AndroidUtils

class Settings : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        AndroidUtils.loadWithGlide(R.drawable.bg_black, bgIV, false)
        setPin.setOnClickListener {
            startActivity(Intent(it.context, SetPinCode::class.java))
        }
    }


    companion object {
        fun newInstance() = Settings()
    }
}