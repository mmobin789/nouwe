package webdealer.nouwe.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_history.*
import webdealer.nouwe.R
import webdealer.nouwe.adapter.HistoryAdapter
import webdealer.nouwe.commonstuff.utils.AndroidUtils

class History : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = HistoryAdapter()
        AndroidUtils.loadWithGlide(R.drawable.white_bg, bgIV, false)
    }


    companion object {
        fun newInstance() = History()
    }
}