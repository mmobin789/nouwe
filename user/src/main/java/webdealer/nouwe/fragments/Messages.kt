package webdealer.nouwe.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_messages.*
import webdealer.nouwe.R
import webdealer.nouwe.adapter.UpcomingAdapter

class Messages : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_messages, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = UpcomingAdapter()
    }


    companion object {
        fun newInstance() = Messages()
    }
}