package webdealer.nouwe.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import webdealer.nouwe.activities.MainActivity

abstract class BaseUI : Fragment() {
    protected lateinit var main: MainActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (activity is MainActivity)
            main = activity as MainActivity
    }

    open fun onBackPressed() {
        main.toolbarDesign("", true)
        main.setFragment(Home.newInstance())
    }

    fun showToast(msg: String) = Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
}