package webdealer.nouwe.fragments

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_cc.*
import webdealer.nouwe.R

class AddCC4 : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_cc, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        cvv()
    }

    private fun cvv() {
        label.text = " CVV"
        et.inputType = InputType.TYPE_CLASS_NUMBER
    }

    companion object {
        fun newInstance() = AddCC4()
    }
}