package webdealer.nouwe.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_cc.*
import webdealer.nouwe.R
import java.util.*

class AddCC3 : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_cc, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        ccExpDate()
    }

    private fun ccExpDate() {
        label.text = " EXPIRATION DATE"
        et.inputType = InputType.TYPE_DATETIME_VARIATION_DATE
        et.isFocusable = false
        et.setOnClickListener {
            chooseDOB(it)
        }
    }

    companion object {
        fun newInstance() = AddCC3()
    }

    private fun chooseDOB(it: View) {

        val calendar = Calendar.getInstance()
        val dpDialog = DatePickerDialog(it.context, R.style.timeUITheme, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            val dob = year.toString() + "/" + (month + 1)
            et.setText(dob)

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        dpDialog.show()
    }
}