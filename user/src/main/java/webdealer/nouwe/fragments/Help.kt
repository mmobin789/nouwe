package webdealer.nouwe.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_help.*
import webdealer.nouwe.R
import webdealer.nouwe.activities.FAQs
import webdealer.nouwe.activities.Legal
import webdealer.nouwe.activities.SubmitComplaint
import webdealer.nouwe.activities.Welcome
import webdealer.nouwe.commonstuff.utils.AndroidUtils

class Help : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_help, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        AndroidUtils.loadWithGlide(R.drawable.white_bg, bgIV, false)

        faqs.setOnClickListener {
            startActivity(Intent(it.context, FAQs::class.java))
        }
        legal.setOnClickListener {
            startActivity(Intent(it.context, Legal::class.java))
        }
        about.setOnClickListener {
            val legalDetail = Intent(it.context, Welcome::class.java)
            legalDetail.putExtra("legal", true)
            legalDetail.putExtra("detail", "ABOUT US")
            startActivity(legalDetail)
        }

        support.setOnClickListener {
            startActivity(Intent(it.context, SubmitComplaint::class.java))
        }
    }


    companion object {
        fun newInstance() = Help()
    }
}