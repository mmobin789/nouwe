package webdealer.nouwe.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.cancel_reason_dialog.*
import kotlinx.android.synthetic.main.congrats_dialog.*
import kotlinx.android.synthetic.main.datepickerdialog.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.need_more_time.*
import kotlinx.android.synthetic.main.promo_dialog.*
import kotlinx.android.synthetic.main.timepickerdialog.*
import kotlinx.android.synthetic.main.tip_menu.*
import kotlinx.android.synthetic.main.va_row.*
import webdealer.nouwe.R
import webdealer.nouwe.activities.Base.Companion.hasLocationPermission
import webdealer.nouwe.activities.Base.Companion.zoomToMyLocation
import webdealer.nouwe.activities.Overview
import webdealer.nouwe.activities.TourCompleted
import webdealer.nouwe.adapter.InfoWindowAdapter
import webdealer.nouwe.commonstuff.utils.AndroidUtils.delay
import webdealer.nouwe.models.Reservation


class Home : BaseUI() {

    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private lateinit var googleMap: GoogleMap
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupMap()
        // val layoutManager = LinearLayoutManager(context)
        // layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        // rv.layoutManager = layoutManager
        // rv.setItemViewCacheSize(5)
        //  rv.adapter = ReservationsAdapter(addReservations())

        close.setOnClickListener {
            reservationUI.visibility = View.VISIBLE
            tipMenu.visibility = View.GONE
        }
        initReservation()
    }

    private fun initReservation() {
        vaRow.setOnLongClickListener {
            detailsUI.visibility = View.VISIBLE
            true
        }
        cancelBooking.setOnClickListener {
            cancelBookingDialog(it.context)
        }

        picIV.setOnClickListener { v ->
            cancelBooking.setBackgroundResource(R.drawable.green_btn)
            cancelBooking.text = "LET'S NOUWE"
            cancelBooking.setOnClickListener {
                it.visibility = View.GONE
                status.text = "IN PROGRESS"
                letsNouweUI.visibility = View.VISIBLE
                addPaymentUI.visibility = View.INVISIBLE
            }
            moreTime.setOnClickListener {
                needTimeDialog()
            }
        }
        tourCompleted.setOnClickListener {
            startActivity(Intent(it.context, TourCompleted::class.java))
        }
        showToast("Click on avatar to see success booking flow.")
    }

    private fun needTimeDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.need_more_time)
        dialog.headerNeed.text = "NEED MORE TIME?"
        dialog.cancelT.setOnClickListener {
            denyNeedTimeDialog()
            dialog.dismiss()
        }
        dialog.send.setOnClickListener {
            dialog.dismiss()
            waitingAgentResponseDialog()
        }
        dialog.show()
    }

    private fun waitingAgentResponseDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.need_more_time)
        dialog.headerNeed.text = "WAITING RESPONSE"
        dialog.contentNeed.text = "Agent has not responded yet.The status will be shown below.By default your request will be set to complete after 2 minutes of no response."
        dialog.progressBar.visibility = View.VISIBLE
        dialog.send.visibility = View.GONE
        dialog.etMinutes.visibility = View.GONE
        dialog.cancelT.setOnClickListener {
            startActivity(Intent(context, TourCompleted::class.java))
            dialog.dismiss()
        }
        dialog.send.visibility = View.GONE
        dialog.show()
        delay(2, Runnable {
            dialog.dismiss()
            startActivity(Intent(context, TourCompleted::class.java))
        })
    }

    private fun denyNeedTimeDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.need_more_time)
        dialog.headerNeed.text = "MORE TIME REQUESTED"
        dialog.contentNeed.text = "You have chosen to deny this request. Are you sure you want to deny this request?"
        dialog.send.text = "YES"
        dialog.cancelT.text = "NO"
        dialog.cancelT.setOnClickListener {
            dialog.dismiss()
        }
        dialog.send.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun cancelBookingDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.congrats_dialog)
        dialog.imageView.setImageResource(R.drawable.error)
        dialog.content.text = "Are you sure you want to cancel this booking?"
        dialog.header.text = "CANCEL BOOKING"
        dialog.feeTV.visibility = View.VISIBLE
        dialog.done.visibility = View.GONE
        dialog.yesNoUI.visibility = View.VISIBLE
        dialog.yes.setTextColor(Color.BLACK)
        dialog.yes.setOnClickListener {
            dialog.dismiss()
            cancelReasonDialog(it.context)
        }
        dialog.no.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

    }

    private fun cancelReasonDialog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.cancel_reason_dialog)
        dialog.noC.setOnClickListener {
            dialog.dismiss()
        }
        dialog.yesC.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun addReservations(): List<Reservation> {
        val list = mutableListOf<Reservation>()
        val reservation = Reservation()
        list.add(Reservation().setActive(true))
        list.add(reservation)
        list.add(reservation)
        list.add(reservation)
        list.add(reservation)
        return list

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
            showCurrentLocation()
        else showToast("Location Permission is Required.")
    }


    @SuppressLint("MissingPermission")
    private fun showCurrentLocation() {
        googleMap.isMyLocationEnabled = true
        googleMap.uiSettings.setAllGesturesEnabled(true)
        googleMap.uiSettings.isMyLocationButtonEnabled = false
        getUserLocation()

    }

    private fun datePickerDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.datepickerdialog)
        dialog.show()
        dialog.next.setOnClickListener {
            dialog.dismiss()
            timePickerDialog()
        }
        dialog.cancel.setOnClickListener {
            dialog.dismiss()
        }

    }

    private fun timePickerDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.timepickerdialog)
        dialog.show()
        dialog.lets.setOnClickListener {
            reviewDialog()
            dialog.dismiss()
        }
        dialog.cancelLets.setOnClickListener {
            dialog.dismiss()
            datePickerDialog()
        }
    }

    private fun reviewDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.promo_dialog)
        dialog.show()
        dialog.cancel1.setOnClickListener {
            dialog.dismiss()
            timePickerDialog()
        }
        dialog.review.setOnClickListener {
            startActivity(Intent(it.context, Overview::class.java))
            dialog.dismiss()
        }
    }
//        val calendar = Calendar.getInstance()
//        val mTimePicker = TimePickerDialog(context, R.style.timeUITheme, TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
//        }, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), false)//Yes 24 hour time
//        mTimePicker.setTitle("Select Time")
//        mTimePicker.show()


    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(main)
        val locationRequest = LocationRequest.create()
        locationRequest.interval = 20000
        locationRequest.fastestInterval = 10000
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        fusedLocationProviderClient!!.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private val locationCallback = object : LocationCallback() {

        override fun onLocationResult(p0: LocationResult?) {
            if (p0 != null) {
                googleMap.clear()
                val lat = p0.lastLocation.latitude
                val lng = p0.lastLocation.longitude
                zoomToMyLocation(googleMap, lat, lng)
                updateMap(LatLng(lat, lng))
            }
        }

    }

    private fun updateMap(latLng: LatLng) {
        googleMap.setInfoWindowAdapter(InfoWindowAdapter(context!!))
        googleMap.addMarker(MarkerOptions().position(latLng))
        googleMap.setOnInfoWindowClickListener {
            it.hideInfoWindow()
            datePickerDialog()
        }

    }

    private fun destroyUI() {
        if (fusedLocationProviderClient != null)
            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback)
    }

    override fun onDestroy() {
        destroyUI()
        super.onDestroy()

    }


    private fun setupMap() {

        map.onCreate(null)
        map.getMapAsync {
            map.onResume()
            it.uiSettings.setAllGesturesEnabled(true)
            it.uiSettings.isZoomControlsEnabled = true
            googleMap = it
            if (hasLocationPermission(main)) {
                showCurrentLocation()
            }


        }
    }

    override fun onBackPressed() {
        if (reservationUI.isShown) {
            reservationUI.visibility = View.GONE
        } else
            main.finish()
    }

    companion object {

        fun newInstance() = Home()
    }
}