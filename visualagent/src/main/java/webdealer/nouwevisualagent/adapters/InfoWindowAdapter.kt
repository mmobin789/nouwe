package webdealer.nouwevisualagent.adapters

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import webdealer.nouwevisualagent.R

class InfoWindowAdapter(private val context: Context) : GoogleMap.InfoWindowAdapter {
    override fun getInfoContents(p0: Marker?): View {
        val textView = TextView(context)
        return textView
    }

    override fun getInfoWindow(p0: Marker?): View {
        val imageView = ImageView(context)
        //      val layoutParams = ViewGroup.LayoutParams(100, 100)
        //    imageView.layoutParams = layoutParams
        imageView.setImageResource(R.drawable.infowindow)
        // imageView.scaleType = ImageView.ScaleType.CENTER_CROP
        return imageView
    }
}