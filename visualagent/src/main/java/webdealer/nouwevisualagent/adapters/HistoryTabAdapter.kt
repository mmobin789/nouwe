package webdealer.nouwevisualagent.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import webdealer.nouwevisualagent.fragments.BaseUI
import webdealer.nouwevisualagent.fragments.TodayTourHistory
import webdealer.nouwevisualagent.fragments.WeeklyTourHistory

class HistoryTabAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): BaseUI {
        return when (position) {
            0 -> TodayTourHistory.newInstance()
            else -> WeeklyTourHistory.newInstance()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0)
            "TODAY"
        else "WEEKLY"
    }
}