package webdealer.nouwevisualagent.activities

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_auth.*
import webdealer.nouwevisualagent.R

class Auth : Base() {
    override fun init() {
        login.setOnClickListener {
            startActivity(Intent(this, Login::class.java))
        }
        signup.setOnClickListener {
            startActivity(Intent(this, SignUp::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_auth)
        super.onCreate(savedInstanceState)

    }
}
