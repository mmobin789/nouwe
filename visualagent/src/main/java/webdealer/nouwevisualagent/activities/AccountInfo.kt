package webdealer.nouwevisualagent.activities

import android.app.Dialog
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.Window
import kotlinx.android.synthetic.main.activity_account_info.*
import kotlinx.android.synthetic.main.congrats_dialog.*
import kotlinx.android.synthetic.main.message_dialog.*
import kotlinx.android.synthetic.main.need_more_time.*
import kotlinx.android.synthetic.main.terms_conditions_dialog.*
import webdealer.nouwe.commonstuff.utils.AndroidUtils
import webdealer.nouwevisualagent.R

class AccountInfo : Base() {
    private var fromSettings = false
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_account_info)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        setToolbarTitle("ACCOUNT INFORMATION")
        fromSettings = intent.getBooleanExtra("settings", false)

        next.setOnClickListener {
            showTermsDialog(it)
        }

        if (fromSettings) {
            setToolbarTitle("BANKING")
            next.text = "SAVE CHANGES"
            next.setOnClickListener {
                confirmationDialog(it)
            }
        }
    }

    override fun close(v: View) {
        if (fromSettings) {
            unsavedChangesDialog(v)
        } else
            super.close(v)
    }

    private fun unsavedChangesDialog(v: View) {
        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.message_dialog)
        dialog.doneD.visibility = View.GONE
        dialog.yesNoUIM.visibility = View.VISIBLE
        dialog.headerM.text = "UNSAVED CHANGES"
        dialog.iv.setImageResource(R.drawable.alert)
        dialog.msg.text = "You have some unsaved changes! Do you want to continue without changes?"
        dialog.yesM.setTextColor(ContextCompat.getColor(v.context, R.color.colorAccent))
        dialog.noM.setOnClickListener {
            dialog.dismiss()

        }
        dialog.yesM.setOnClickListener {
            dialog.dismiss()
            super.close(it)
        }
        dialog.show()
    }

    private fun confirmationDialog(v: View) {
        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.message_dialog)
        dialog.doneD.visibility = View.GONE
        dialog.yesNoUIM.visibility = View.VISIBLE
        dialog.headerM.text = "CONFIRMATION"
        dialog.iv.setImageResource(R.drawable.alert)
        dialog.msg.text = "Your recent edits will cause your account to be unverified until you verify your new banking information.Do you wish to continue?"
        dialog.yesM.setTextColor(ContextCompat.getColor(v.context, R.color.colorAccent))
        dialog.noM.setOnClickListener {
            dialog.dismiss()
        }
        dialog.yesM.setOnClickListener {
            dialog.dismiss()
            successDialog()
        }
        dialog.show()
    }

    private fun successDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.need_more_time)
        dialog.ivNeed.setImageResource(R.drawable.greentick)
        dialog.headerNeed.text = "SUCCESS"
        dialog.contentNeed.text = "Please verify your new information through the Settings page.You are now being redirected to your Account."
        dialog.etMinutes.visibility = View.GONE
        dialog.btns.visibility = View.GONE
        dialog.progressBar.visibility = View.VISIBLE
        dialog.show()
        AndroidUtils.delay(3, Runnable {
            dialog.dismiss()
            onBackPressed()
        })
    }

    private fun showTermsDialog(v: View) {
        setToolbarTitle("Agreement")
        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.terms_conditions_dialog)
        dialog.show()
        dialog.ok.setOnClickListener {
            dialog.dismiss()
            successDialog(it)
        }
        dialog.setOnCancelListener {
            setToolbarTitle("Account Information")
        }

    }

    private fun successDialog(v: View) {
        setToolbarTitle("Success")
        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.congrats_dialog)
        dialog.show()
        dialog.done.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setOnCancelListener {
            setToolbarTitle("Verification")
        }
    }
}
