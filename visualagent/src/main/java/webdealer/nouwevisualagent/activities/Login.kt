package webdealer.nouwevisualagent.activities

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.*
import webdealer.nouwevisualagent.R

class Login : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_login)
        super.onCreate(savedInstanceState)


    }

    override fun init() {
        setToolbarTitle("Login")
        login.setOnClickListener {
            startActivity(Intent(it.context, MainActivity::class.java))
        }
        forgotPassword.setOnClickListener {
            startActivity(Intent(it.context, ForgotPassword::class.java))
        }
    }
}
