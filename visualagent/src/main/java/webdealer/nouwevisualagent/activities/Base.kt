package webdealer.nouwevisualagent.activities

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.toolbar.*
import webdealer.nouwevisualagent.persistence.AppStorage


abstract class Base : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppStorage.init(this)
        init()
    }

    fun setToolbarTitle(title: String) {
        titleTB.text = title
    }

    fun showToast(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()

    open fun close(v: View) = onBackPressed()

    fun zoomToMyLocation(googleMap: GoogleMap, lat: Double, lng: Double) {

        val latLng = LatLng(lat, lng)
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 6f)
        googleMap.animateCamera(cameraUpdate)

    }

    fun hasLocationPermission(base: Base): Boolean {
        var permission = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permission = ActivityCompat.checkSelfPermission(base, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(base, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
            if (!permission) {

                base.requestPermissions(arrayOf(
                        Manifest
                                .permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
                ), 3)
            }
        }
        return permission
    }

    abstract fun init()


}