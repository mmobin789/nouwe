package webdealer.nouwevisualagent.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_settings.*
import webdealer.nouwe.commonstuff.utils.AndroidUtils
import webdealer.nouwevisualagent.R

class Settings : Base() {


    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_settings)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        AndroidUtils.loadWithGlide(R.drawable.bg_black, bgIV, false)
        setPin.setOnClickListener {
            startActivity(Intent(it.context, SetPinCode::class.java))
        }
        verify.setOnClickListener {
            verifiedUI.visibility = View.VISIBLE
            bankingDetails.visibility = View.GONE
        }
    }


}