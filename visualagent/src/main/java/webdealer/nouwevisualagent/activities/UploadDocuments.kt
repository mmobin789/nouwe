package webdealer.nouwevisualagent.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_upload_documents.*
import webdealer.nouwevisualagent.R

class UploadDocuments : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_upload_documents)
        super.onCreate(savedInstanceState)

    }



    override fun init() {
        val uploadDocs = intent.getBooleanExtra("updateDocs", false)
        if (uploadDocs) {
            next.text = "REVIEW"
            setToolbarTitle("DOCUMENTS")
        } else setToolbarTitle("Upload Documents".toUpperCase())
        next.setOnClickListener {
            val review = Intent(it.context, ReviewDocs::class.java)
            if (uploadDocs) {
                setResult(Activity.RESULT_OK)
                review.putExtra("updateDocs", uploadDocs)

            }


            startActivity(review)
        }
    }
}
