package webdealer.nouwevisualagent.activities

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_sign_up.*
import webdealer.nouwe.commonstuff.utils.AndroidUtils
import webdealer.nouwevisualagent.R

class SignUp : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_sign_up)
        super.onCreate(savedInstanceState)


    }

    override fun init() {
        setToolbarTitle("Register")
        next.setOnClickListener {
            startActivity(Intent(it.context, Verification::class.java))
        }
        AndroidUtils.loadWithGlide(R.drawable.white_bg, bgIV, false)
    }
}
