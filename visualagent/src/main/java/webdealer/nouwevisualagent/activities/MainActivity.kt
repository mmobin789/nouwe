package webdealer.nouwevisualagent.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.topbar.*
import webdealer.nouwevisualagent.R
import webdealer.nouwevisualagent.fragments.Account
import webdealer.nouwevisualagent.fragments.BaseUI
import webdealer.nouwevisualagent.fragments.History
import webdealer.nouwevisualagent.fragments.Home

class MainActivity : Base() {
    private lateinit var baseUI: BaseUI
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_main)
        super.onCreate(savedInstanceState)

    }


    override fun init() {
        val support = intent.getBooleanExtra("support", false)

        //    val user = AppStorage.getUser()
        if (support)
            accountSelected(Account.UI.SupportUI)
        else
            homeSelected()


        setTopBar()

    }

    private fun setTopBar() {
        home.setOnClickListener {
            homeSelected()
        }
        account.setOnClickListener {
            accountSelected(Account.UI.AccountUI)
        }
        history.setOnClickListener {
            historySelected()
        }
        postings.setOnClickListener {
            postingsSelected()
        }

        statusSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            val text = if (isChecked)
                "ONLINE"
            else "OFFLINE"
            statusTV.text = text
            statusSelected()
        }
    }

    override fun onBackPressed() {
        if (baseUI is Home)
            super.onBackPressed()
        else baseUI.onBackPressed()
    }

    fun homeSelected() {
        setFragment(Home.newInstance())
        line1.visibility = View.VISIBLE
        line2.visibility = View.INVISIBLE
        line3.visibility = View.INVISIBLE
        line4.visibility = View.INVISIBLE
        line5.visibility = View.INVISIBLE
    }

    private fun historySelected() {
        setFragment(History.newInstance())
        line3.visibility = View.VISIBLE
        line2.visibility = View.INVISIBLE
        line1.visibility = View.INVISIBLE
        line4.visibility = View.INVISIBLE
        line5.visibility = View.INVISIBLE
    }

    fun accountSelected(ui: Account.UI) {
        setFragment(Account.newInstance(ui))
        line1.visibility = View.INVISIBLE
        line2.visibility = View.VISIBLE
        line3.visibility = View.INVISIBLE
        line4.visibility = View.INVISIBLE
        line5.visibility = View.INVISIBLE
    }

    private fun postingsSelected() {
        line1.visibility = View.INVISIBLE
        line2.visibility = View.INVISIBLE
        line3.visibility = View.INVISIBLE
        line4.visibility = View.VISIBLE
        line5.visibility = View.INVISIBLE
    }

    private fun statusSelected() {
        line1.visibility = View.INVISIBLE
        line2.visibility = View.INVISIBLE
        line3.visibility = View.INVISIBLE
        line4.visibility = View.INVISIBLE
        line5.visibility = View.VISIBLE

    }

    fun setFragment(baseUI: BaseUI) {
        this.baseUI = baseUI
        supportFragmentManager.beginTransaction().replace(R.id.container, this.baseUI).commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        baseUI.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        baseUI.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
