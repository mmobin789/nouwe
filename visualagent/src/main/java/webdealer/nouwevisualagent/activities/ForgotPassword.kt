package webdealer.nouwevisualagent.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.message_dialog.*
import webdealer.nouwevisualagent.R

class ForgotPassword : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_forgot_password)
        super.onCreate(savedInstanceState)


    }

    override fun init() {
        setToolbarTitle("Forgot Password")
        reset.setOnClickListener {
            resetPasswordConfirmDialog(it)
        }
    }

    private fun resetPasswordConfirmDialog(v: View) {

        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.message_dialog)
        dialog.show()
        dialog.headerM.text = "SENT"
        dialog.msg.text = getString(R.string.rpass)
        dialog.doneD.setOnClickListener {
            dialog.dismiss()
            startActivity(Intent(it.context, ResetPassword::class.java))
        }

    }
}
