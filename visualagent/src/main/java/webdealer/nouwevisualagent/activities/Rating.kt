package webdealer.nouwevisualagent.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import kotlinx.android.synthetic.main.activity_rating.*
import kotlinx.android.synthetic.main.need_more_time.*
import kotlinx.android.synthetic.main.toolbar.*
import webdealer.nouwe.commonstuff.utils.AndroidUtils.delay
import webdealer.nouwevisualagent.R

class Rating : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_rating)
        super.onCreate(savedInstanceState)

    }

    override fun init() {
        setToolbarTitle("REVIEW")
        back.visibility = View.INVISIBLE
        rate.setOnClickListener {
            successDialog()
        }
        help.setOnClickListener {

            val account = Intent(it.context, MainActivity::class.java)
            account.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
            account.putExtra("support", true)
            startActivity(account)
        }


    }

    private fun successDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.need_more_time)
        dialog.ivNeed.setImageResource(R.drawable.greentick)
        dialog.headerNeed.text = "SUCCESS"
        dialog.contentNeed.text = "You are being redirected back to the home page."
        dialog.etMinutes.visibility = View.GONE
        dialog.btns.visibility = View.GONE
        dialog.progressBar.visibility = View.VISIBLE
        dialog.show()
        delay(2, Runnable {
            dialog.dismiss()
            onBackPressed()
        })
    }
}
