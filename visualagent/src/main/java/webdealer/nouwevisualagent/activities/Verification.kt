package webdealer.nouwevisualagent.activities

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_verification.*
import webdealer.nouwevisualagent.R

class Verification : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_verification)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        setToolbarTitle("Verification")
        submit.setOnClickListener {
            startActivity(Intent(it.context, UploadDocuments::class.java))
        }
    }

}
