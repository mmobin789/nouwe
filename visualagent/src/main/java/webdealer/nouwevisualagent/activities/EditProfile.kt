package webdealer.nouwevisualagent.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.change_password_dialog.*
import webdealer.nouwevisualagent.R

class EditProfile : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_edit_profile)
        super.onCreate(savedInstanceState)

    }

    override fun init() {
        etPassword.setOnClickListener {
            changePasswordDialog(it)
        }
        etPhone.setOnClickListener {
            startActivity(Intent(it.context, UpdatePhone::class.java))
        }
        setToolbarTitle("EDIT PROFILE")
    }

    private fun changePasswordDialog(v: View) {
        val dialog = Dialog(v.context)
        dialog.setCancelable(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.change_password_dialog)
        dialog.done.setOnClickListener {

        }
        dialog.cancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
}
