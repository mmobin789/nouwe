package webdealer.nouwevisualagent.activities

import android.content.Intent
import android.os.Bundle
import webdealer.nouwe.commonstuff.utils.AndroidUtils.delay
import webdealer.nouwevisualagent.R

class Splash : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_splash)
        super.onCreate(savedInstanceState)


    }

    override fun init() {

        delay(3, Runnable {

            startActivity(Intent(this, Auth::class.java))
        })
    }
}
