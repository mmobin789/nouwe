package webdealer.nouwevisualagent.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import kotlinx.android.synthetic.main.activity_review_docs.*
import kotlinx.android.synthetic.main.need_more_time.*
import webdealer.nouwe.commonstuff.utils.AndroidUtils
import webdealer.nouwevisualagent.R

class ReviewDocs : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_review_docs)
        super.onCreate(savedInstanceState)
    }

    override fun init() {
        setToolbarTitle("REVIEW DOCUMENTS")
        val updateMode = intent.getBooleanExtra("update", false)
        val uploadDocs = intent.getBooleanExtra("updateDocs", false)
        next.setOnClickListener {
            val intent = Intent()
            val ui = if (updateMode) {
                intent.putExtra("updateDocs", true)
                UploadDocuments::class.java
            } else
                AccountInfo::class.java
            intent.setClass(it.context, ui)
            startActivity(intent)
        }
        if (updateMode) {
            next.setBackgroundResource(R.drawable.gray_btn)
            next.text = "UPDATE DOCUMENTS"
            setToolbarTitle("DOCUMENTS")
        } else if (uploadDocs) {
            setToolbarTitle("DOCUMENTS")
            next.text = "SUBMIT"
            next.setOnClickListener {
                successDialog()
            }
        }

    }


    private fun successDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.need_more_time)
        dialog.ivNeed.setImageResource(R.drawable.greentick)
        dialog.headerNeed.text = "SUCCESS"
        dialog.contentNeed.text = "Please allow us to review the documents recently updated.Once verified,your account will be re-verified.Thank you."
        dialog.etMinutes.visibility = View.GONE
        dialog.btns.visibility = View.GONE
        dialog.progressBar.visibility = View.VISIBLE
        dialog.show()
        AndroidUtils.delay(2, Runnable {
            dialog.dismiss()
            setResult(Activity.RESULT_OK)
            finish()
        })
    }
}
