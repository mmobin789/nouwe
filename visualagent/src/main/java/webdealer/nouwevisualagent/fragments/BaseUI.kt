package webdealer.nouwevisualagent.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import webdealer.nouwevisualagent.activities.MainActivity

abstract class BaseUI : Fragment() {

    protected lateinit var main: MainActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (activity is MainActivity)
            main = activity as MainActivity
    }

    fun showToast(msg: String) = Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

    open fun onBackPressed() {
        //   main.toolbarDesign("", true)
        main.homeSelected()
    }


}