package webdealer.nouwevisualagent.fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import kotlinx.android.synthetic.main.activity_submit_complaint.*
import kotlinx.android.synthetic.main.fragment_account.*
import kotlinx.android.synthetic.main.message_dialog.*
import webdealer.nouwe.commonstuff.utils.AndroidUtils
import webdealer.nouwevisualagent.R
import webdealer.nouwevisualagent.activities.AccountInfo
import webdealer.nouwevisualagent.activities.EditProfile
import webdealer.nouwevisualagent.activities.ReviewDocs
import webdealer.nouwevisualagent.activities.Settings

class Account : BaseUI() {
    private var requestMode = "AccountUI"
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requestMode = arguments!!.getString("ui")
        if (requestMode == UI.RequestUI.name) {
            submitRequestUI.visibility = View.VISIBLE
            accountUI.visibility = View.GONE
            submit.setOnClickListener {
                successDialog()
            }
        } else if (requestMode == UI.SupportUI.name) {
            submitRequestUI.visibility = View.VISIBLE
            accountUI.visibility = View.GONE
            submit.setOnClickListener {
                showToast(" support Message")

            }

        }
        settings.setOnClickListener {
            startActivity(Intent(it.context, Settings::class.java))
        }
        AndroidUtils.loadWithGlide(R.drawable.white_bg, bgIV, false)

        docs.setOnClickListener {
            val update = Intent(it.context, ReviewDocs::class.java)
            update.putExtra("update", true)
            startActivity(update)
        }
        help.setOnClickListener {
            main.setFragment(Help.newInstance())
        }
        editProfileTV.setOnClickListener {
            startActivity(Intent(it.context, EditProfile::class.java))
        }

        banking.setOnClickListener {
            val accountInfo = Intent(it.context, AccountInfo::class.java)
            accountInfo.putExtra("settings", true)
            startActivity(accountInfo)
        }
        logout.setOnClickListener {
            main.close(it)
        }

    }

    private fun successDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.message_dialog)
        dialog.iv.setImageResource(R.drawable.greentick)
        dialog.headerM.text = "SUCCESS"
        dialog.msg.text = "You have successfully submitted your form to Nouwe.We will be sure to get back with you shortly.Thank you."
        dialog.show()
        dialog.doneD.visibility = View.INVISIBLE
        // after 8 sec hide request UI and show account UI
        AndroidUtils.delay(3, Runnable {
            dialog.dismiss()
            submitRequestUI.visibility = View.GONE
            accountUI.visibility = View.VISIBLE
        })
    }

    override fun onBackPressed() {
        if (requestMode == UI.RequestUI.name)
            main.setFragment(History.newInstance())
        else main.homeSelected()
    }

    companion object {
        fun newInstance(ui: UI): Account {
            val account = Account()
            val args = Bundle()
            args.putString("ui", ui.name)
            account.arguments = args
            return account
        }
    }

    enum class UI {
        RequestUI, SupportUI, AccountUI
    }
}