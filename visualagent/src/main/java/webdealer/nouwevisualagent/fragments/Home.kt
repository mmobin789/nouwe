package webdealer.nouwevisualagent.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.need_more_time.*
import kotlinx.android.synthetic.main.overviewmenu.*
import kotlinx.android.synthetic.main.tour_request_dialog.*
import webdealer.nouwe.commonstuff.utils.AndroidUtils
import webdealer.nouwe.commonstuff.utils.AndroidUtils.delay
import webdealer.nouwevisualagent.R
import webdealer.nouwevisualagent.activities.TourCompleted
import webdealer.nouwevisualagent.adapters.InfoWindowAdapter

class Home : BaseUI() {

    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private lateinit var googleMap: GoogleMap
    private lateinit var marker: Marker

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupMap()

    }

    private fun moreTimeDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.need_more_time)
        dialog.headerNeed.text = "MORE TIME REQUESTED"
        dialog.cancelT.setOnClickListener {

            denyNeedTimeDialog()

        }
        dialog.send.setOnClickListener {
            dialog.dismiss()
            main.startActivityForResult(Intent(context, TourCompleted::class.java), 4)
            // tour summary
        }
        dialog.cancelT.text = "DENY"
        dialog.send.text = "ACCEPT"
        dialog.contentNeed.text = "John smith, from your recent tour, has requested 10 more minutes."
        dialog.etMinutes.visibility = View.GONE
        dialog.show()
    }

    private fun waitingAgentResponseDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.need_more_time)
        dialog.headerNeed.text = "WAITING RESPONSE"
        dialog.contentNeed.text = "Agent has not responded yet.The status will be shown below.By default your request will be set to complete after 2 minutes of no response."
        dialog.progressBar.visibility = View.VISIBLE
        dialog.send.visibility = View.GONE
        dialog.etMinutes.visibility = View.GONE
        dialog.cancelT.setOnClickListener {
            dialog.dismiss()
        }
        dialog.send.visibility = View.GONE
        dialog.show()
        delay(2, Runnable {
            dialog.dismiss()
            main.startActivityForResult(Intent(context, TourCompleted::class.java), 4)
        })
    }

    private fun denyNeedTimeDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.need_more_time)
        dialog.headerNeed.text = "MORE TIME REQUESTED"
        dialog.contentNeed.text = "You have chosen to deny this request. Are you sure you want to deny this request?"
        dialog.send.text = "YES"
        dialog.cancelT.text = "NO"
        dialog.etMinutes.visibility = View.GONE
        dialog.cancelT.setOnClickListener {
            dialog.dismiss()
        }
        dialog.send.setOnClickListener {
            dialog.dismiss()
            main.startActivity(Intent(it.context, TourCompleted::class.java))
        }
        dialog.show()
    }


    private fun setupMap() {
        map.onCreate(null)
        map.getMapAsync {
            map.onResume()
            it.uiSettings.setAllGesturesEnabled(true)
            it.uiSettings.isZoomControlsEnabled = true
            googleMap = it
            if (main.hasLocationPermission(main)) {
                showCurrentLocation()

            }


        }
    }

    private fun tourRequestDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.tour_request_dialog)
        AndroidUtils.loadWithGlide(R.drawable.white_bg, dialog.root)
        dialog.setCancelable(false)
        dialog.show()
        dialog.accept.setOnClickListener {
            dialog.timerUI.visibility = View.INVISIBLE
            dialog.messageUI.visibility = View.VISIBLE
            dialog.exit.visibility = View.VISIBLE
            dialog.yesNoUI.visibility = View.GONE
            dialog.tourLabel.setTextColor(ContextCompat.getColor(it.context, R.color.colorAccent))
            dialog.tourLabel.text = "TOUR ACCEPTED"


        }
        dialog.reject.setOnClickListener {
            dialog.dismiss()
        }
        dialog.exit.setOnClickListener {
            dialog.dismiss()
            onGoing()
        }
        dialog.mapView.onCreate(null)

        dialog.mapView.getMapAsync {
            dialog.mapView.onResume()
            it.uiSettings.setAllGesturesEnabled(true)
            it.uiSettings.isZoomControlsEnabled = true
            it.setInfoWindowAdapter(InfoWindowAdapter(context!!))
            val dMarker = it.addMarker(MarkerOptions().position(marker.position))
            main.zoomToMyLocation(it, marker.position.latitude, marker.position.longitude)
            dMarker.showInfoWindow()


        }
    }


    private fun onGoing() {
        overviewUI.visibility = View.GONE
        upcomingUI.visibility = View.VISIBLE
        destinationUI.visibility = View.VISIBLE
        labelEarned1.text = "UNTIL START"
        remainingTimeTV.visibility = View.GONE
        delay(3, Runnable {
            header2.text = "ONGOING"

            marker.showInfoWindow()
            delay(3, Runnable {
                btnsUI.visibility = View.VISIBLE

            })
        })

        startTour.setOnClickListener { v ->
            startTour.text = "TOUR COMPLETED"
            moretime.visibility = View.VISIBLE
            startTour.setOnClickListener {
                // tour completed
                main.startActivity(Intent(it.context, TourCompleted::class.java))

            }
            moretime.visibility = View.VISIBLE

            moretime.setOnClickListener {
                needTimeDialog()
            }
        }
        delay(4, Runnable { moreTimeDialog() })   // if user made request

    }

    private fun needTimeDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.need_more_time)
        dialog.headerNeed.text = "NEED MORE TIME?"
        dialog.cancelT.setOnClickListener {

            dialog.dismiss()
        }
        dialog.send.setOnClickListener {
            dialog.dismiss()
            waitingAgentResponseDialog()
        }
        dialog.show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
            showCurrentLocation()
        else showToast("Location Permission is Required.")
    }

    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(main)
        val locationRequest = LocationRequest.create()
        locationRequest.interval = 20000
        locationRequest.fastestInterval = 10000
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        fusedLocationProviderClient!!.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private val locationCallback = object : LocationCallback() {

        override fun onLocationResult(p0: LocationResult?) {
            if (p0 != null) {
                val lat = p0.lastLocation.latitude
                val lng = p0.lastLocation.longitude
                main.zoomToMyLocation(googleMap, lat, lng)
                updateMap(LatLng(lat, lng))

            }
        }

    }

    @SuppressLint("MissingPermission")
    private fun showCurrentLocation() {

        googleMap.isMyLocationEnabled = true
        googleMap.uiSettings.setAllGesturesEnabled(true)
        googleMap.uiSettings.isMyLocationButtonEnabled = false
        getUserLocation()
    }


    private fun updateMap(latLng: LatLng) {
        googleMap.setInfoWindowAdapter(InfoWindowAdapter(context!!))
        marker = googleMap.addMarker(MarkerOptions().position(latLng))
        googleMap.setOnInfoWindowClickListener {
            it.hideInfoWindow()

        }
        googleMap.setOnMarkerClickListener {
            tourRequestDialog()
            true
        }

    }

    private fun destroyUI() {
        if (fusedLocationProviderClient != null)
            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback)
    }

    override fun onDestroy() {
        destroyUI()
        super.onDestroy()
    }

    companion object {
        fun newInstance() = Home()
    }
}