package webdealer.nouwevisualagent.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_history_today.*
import webdealer.nouwevisualagent.R
import webdealer.nouwevisualagent.adapters.TourHistoryAdapter

class WeeklyTourHistory : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_history_weekly, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = TourHistoryAdapter()

    }


    companion object {
        fun newInstance() = WeeklyTourHistory()
    }
}