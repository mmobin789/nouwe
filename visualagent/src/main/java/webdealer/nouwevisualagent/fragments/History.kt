package webdealer.nouwevisualagent.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_history.*
import webdealer.nouwevisualagent.R
import webdealer.nouwevisualagent.adapters.HistoryTabAdapter

class History : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        pager.adapter = HistoryTabAdapter(childFragmentManager)
        tabL.setupWithViewPager(pager)

    }


    companion object {
        fun newInstance() = History()
    }
}