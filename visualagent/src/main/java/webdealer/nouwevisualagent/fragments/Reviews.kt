package webdealer.nouwevisualagent.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_reviews.*
import webdealer.nouwevisualagent.R
import webdealer.nouwevisualagent.adapters.ReviewsAdapter

class Reviews : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_reviews, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        backToHistory.setOnClickListener {
            onBackPressed()
        }
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = ReviewsAdapter()
    }

    override fun onBackPressed() {
        main.setFragment(History.newInstance())
    }

    companion object {
        fun newInstance() = Reviews()
    }
}