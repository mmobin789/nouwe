package webdealer.nouwe.commonstuff.utils

import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import android.util.Patterns
import id.zelory.compressor.Compressor
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

object Utils {
    fun isValidEmail(email: String): Boolean {
        return email.isNotBlank() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    // if compression fails return the file intact.


    fun getCompressedFile(context: Context, file: File): File {
        try {
            return Compressor(context)
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setCompressFormat(Bitmap.CompressFormat.PNG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).absolutePath)
                    .compressToFile(file)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return file
    }

    fun getCurrentTime(): String {
        val sdf = SimpleDateFormat("hh:mm a", Locale.getDefault())
        return sdf.format(Date())
    }

    fun getMaskedCardNumber(cardNumber: String): String {
        return if (cardNumber.isNotBlank()) {
            val last4digits = cardNumber.substring(cardNumber.length - 4, cardNumber.length)

            val x = StringBuilder()
            for (i in 0..cardNumber.length - 4) {
                x.append("*")
            }
            x.toString() + last4digits
        } else cardNumber
    }
}