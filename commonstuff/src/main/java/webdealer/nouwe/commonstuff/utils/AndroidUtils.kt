package webdealer.nouwe.commonstuff.utils

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Handler
import android.support.annotation.DrawableRes
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import webdealer.nouwe.commonstuff.R
import java.util.concurrent.TimeUnit


object AndroidUtils {

    fun delay(seconds: Long, runnable: Runnable) {
        Handler().postDelayed(runnable, TimeUnit.SECONDS.toMillis(seconds))
    }

    fun getSpannableText(s: String): SpannableString {
        val spannable = SpannableString(s)
        spannable.setSpan(ForegroundColorSpan(Color.parseColor("#5d8ca8")), s.indexOf("H"), s.indexOf("H") + 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable.setSpan(ForegroundColorSpan(Color.parseColor("#5d8ca8")), s.indexOf("H"), s.indexOf("H") + 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spannable
    }


    fun loadWithGlide(path: String, iv: ImageView, isCircle: Boolean) {
        val request = GlideApp.with(iv.context).load(path).placeholder(R.drawable.white_bg)
        if (isCircle)
            request.apply(RequestOptions.circleCropTransform())
        request.into(iv)
    }

    fun loadWithGlide(resID: Int, iv: ImageView, isCircle: Boolean) {
        val request = GlideApp.with(iv.context).load(resID).placeholder(R.drawable.white_bg)
        if (isCircle)
            request.apply(RequestOptions.circleCropTransform())
        request.into(iv)
    }

    fun loadWithGlide(@DrawableRes resID: Int, v: View) {
        GlideApp.with(v.context).load(resID).into(object : SimpleTarget<Drawable>() {
            override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                v.background = resource
            }


        })
    }

}